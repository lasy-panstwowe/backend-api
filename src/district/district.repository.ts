import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { MongoRepository } from '../database/repositories/mongo.repository';
import { Model, Types } from 'mongoose';
import { District, DistrictDocument } from './schemas/district.schema';
import { CreateDistrictDto } from './dto/create-district.dto';

@Injectable()
export class DistrictRepository extends MongoRepository<DistrictDocument> {
  constructor(
    @InjectModel(District.name)
    private readonly districtModel: Model<DistrictDocument>,
  ) {
    super(districtModel);
  }
  createDistrict(createDistrictDto: CreateDistrictDto) {
    const { name, forestryInspectorateId } = createDistrictDto;
    return this.create({
      name,
      forestryInspectorate: forestryInspectorateId,
      _id: new Types.ObjectId(),
    });
  }
}
