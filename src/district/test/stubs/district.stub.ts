import { District } from 'src/district/schemas/district.schema';

export const districtStub = (): District => {
  return {
    name: 'district stub',
    forestryInspectorate: '6260182db887cd308cdaaa19',
  };
};
