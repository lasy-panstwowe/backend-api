import { Module } from '@nestjs/common';
import { DistrictService } from './district.service';
import { DistrictController } from './district.controller';
import { District, DistrictSchema } from './schemas/district.schema';
import { MongooseModule } from '@nestjs/mongoose';
import { DistrictRepository } from './district.repository';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: District.name, schema: DistrictSchema },
    ]),
  ],
  controllers: [DistrictController],
  providers: [DistrictService, DistrictRepository],
  exports: [DistrictService],
})
export class DistrictModule {}
