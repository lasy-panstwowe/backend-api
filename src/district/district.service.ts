import { Injectable } from '@nestjs/common';
import { CreateDistrictDto } from './dto/create-district.dto';
import { UpdateDistrictDto } from './dto/update-district.dto';
import { DistrictRepository } from './district.repository';
import { IDistrict } from './interfaces/district.interface';

@Injectable()
export class DistrictService implements IDistrict {
  constructor(private readonly districtRepository: DistrictRepository) {}
  create(createDistrictDto: CreateDistrictDto) {
    return this.districtRepository.createDistrict(createDistrictDto);
  }

  findAll() {
    return this.districtRepository.find();
  }

  findById(id: string) {
    return this.districtRepository.findById(id);
  }

  update(id: string, updateDistrictDto: UpdateDistrictDto) {
    return this.districtRepository.findByIdAndUpdate(id, updateDistrictDto);
  }

  remove(id: string) {
    return this.districtRepository.findByIdAndDelete(id);
  }
}
