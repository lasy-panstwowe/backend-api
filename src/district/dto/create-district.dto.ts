import { IsMongoId, IsString } from 'class-validator';

export class CreateDistrictDto {
  @IsString()
  name: string;
  @IsMongoId()
  forestryInspectorateId: string;
}
