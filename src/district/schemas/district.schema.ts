import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { Document } from 'mongoose';
import { ForestryInspectorateCollectionName } from '../../forestry_inspectorate/schemas/forestry_inspectorate.schema';

export const DistrictCollectionName = 'District';

export type DistrictDocument = District & Document;

@Schema()
export class District {
  @Prop({ required: true })
  name: string;
  @Prop({
    required: true,
    ref: ForestryInspectorateCollectionName,
    type: mongoose.Schema.Types.ObjectId,
  })
  forestryInspectorate: string;
}

export const DistrictSchema = SchemaFactory.createForClass(District);
