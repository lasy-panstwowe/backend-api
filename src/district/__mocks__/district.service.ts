import { districtStub } from '../test/stubs/district.stub';

export const DistrictService = jest.fn().mockReturnValue({
  findById: jest.fn().mockResolvedValue(districtStub()),
});
