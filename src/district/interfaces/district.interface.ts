import { CreateDistrictDto } from '../dto/create-district.dto';
import { UpdateDistrictDto } from '../dto/update-district.dto';
import { DistrictDocument } from '../schemas/district.schema';

export interface IDistrict {
  create(createDistrictDto: CreateDistrictDto): Promise<DistrictDocument>;
  findAll(): Promise<DistrictDocument[]>;
  findById(id: string): Promise<DistrictDocument>;
  update(
    id: string,
    updateForestryDto: UpdateDistrictDto,
  ): Promise<DistrictDocument>;
  remove(id: string): Promise<DistrictDocument>;
}
