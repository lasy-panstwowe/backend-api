import {
  BadRequestException,
  Injectable,
  InternalServerErrorException,
} from '@nestjs/common';
import { DistrictService } from '../district/district.service';
import { CreateForestryDto } from './dto/create.forestry.dto';
import { UpdateForestryDto } from './dto/update.forestry.dto';
import { ForestryRepository } from './forestry.repository';
import { IForestry } from './interfaces/forestry.interface';
import { Point } from './schemas/forestry.schema';

@Injectable()
export class ForestryService implements IForestry {
  constructor(
    private readonly forestryRepository: ForestryRepository,
    private readonly districtService: DistrictService,
  ) {}
  async create(createForestryDto: CreateForestryDto) {
    const { coords } = createForestryDto;
    const uniqueLats = new Set(coords.map((coord) => coord.lat));
    const uniqueLngs = new Set(coords.map((coord) => coord.lng));
    const invalidCoord = coords.find(
      (coord) =>
        coord.lat > 90 ||
        coord.lat < -90 ||
        coord.lng > 180 ||
        coord.lng < -180,
    );
    if (coords.length < 3) {
      throw new BadRequestException(
        'At least three coordination points are required',
      );
    }
    if (invalidCoord) {
      throw new BadRequestException(
        `Invalid coordinates detected in coordination point: {lat: ${invalidCoord.lat}, lng: ${invalidCoord.lng}}. Correct latitude range: [-90, 90]. Correct longitude range: [-180, 180]`,
      );
    }
    if (uniqueLats.size < coords.length && uniqueLngs.size < coords.length) {
      throw new BadRequestException('Duplicate coordination points detected');
    }
    const district = await this.districtService.findById(
      createForestryDto.districtId,
    );
    if (!district) throw new InternalServerErrorException('District not found');
    const duplicatedCoords = await this.findByCoords(coords);
    if (duplicatedCoords) {
      throw new InternalServerErrorException(
        'There is already a forestry with exactly the same coords',
      );
    }
    return this.forestryRepository.createForestry(createForestryDto);
  }

  async findByCoords(coords: Point[]) {
    return this.forestryRepository.findByCoords(coords);
  }

  findAll() {
    return this.forestryRepository.find();
  }

  findById(id: string) {
    return this.forestryRepository.findById(id);
  }

  update(id: string, updateForestryDto: UpdateForestryDto) {
    return this.forestryRepository.findByIdAndUpdate(id, updateForestryDto);
  }

  remove(id: string) {
    return this.forestryRepository.findByIdAndDelete(id);
  }
}
