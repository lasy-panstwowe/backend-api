import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { CreateForestryDto } from './dto/create.forestry.dto';
import { UpdateForestryDto } from './dto/update.forestry.dto';
import { ForestryService } from './forestry.service';

@Controller('forestry')
export class ForestryController {
  constructor(private readonly forestryService: ForestryService) {}

  @Post()
  create(@Body() createForestryDto: CreateForestryDto) {
    return this.forestryService.create(createForestryDto);
  }

  @Get()
  findAll() {
    return this.forestryService.findAll();
  }

  @Get(':id')
  findById(@Param('id') id: string) {
    return this.forestryService.findById(id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateForestryDto: UpdateForestryDto,
  ) {
    return this.forestryService.update(id, updateForestryDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.forestryService.remove(id);
  }
}
