import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { MongoRepository } from '../database/repositories/mongo.repository';
import { Forestry, ForestryDocument, Point } from './schemas/forestry.schema';
import { Model, Types } from 'mongoose';
import { CreateForestryDto } from './dto/create.forestry.dto';

@Injectable()
export class ForestryRepository extends MongoRepository<ForestryDocument> {
  constructor(
    @InjectModel(Forestry.name)
    private readonly forestryModel: Model<ForestryDocument>,
  ) {
    super(forestryModel);
  }
  async findByCoords(coords: Point[]) {
    return this.findOne({ coords: coords });
  }
  async createForestry(createForestryDto: CreateForestryDto) {
    const { districtId, coords, ...rest } = createForestryDto;
    return this.create({
      district: districtId,
      coords,
      ...rest,
      _id: new Types.ObjectId(),
    });
  }
}
