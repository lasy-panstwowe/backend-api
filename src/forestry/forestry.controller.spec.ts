import { Test, TestingModule } from '@nestjs/testing';
import { CreateForestryDto } from './dto/create.forestry.dto';
import { UpdateForestryDto } from './dto/update.forestry.dto';
import { ForestryController } from './forestry.controller';
import { ForestryService } from './forestry.service';
import { Forestry } from './schemas/forestry.schema';
import { forestryStub } from './test/stubs/forestry.stub';
jest.mock('./forestry.service');

describe('ForestryController', () => {
  let controller: ForestryController;
  let service: ForestryService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ForestryController],
      providers: [ForestryService],
    }).compile();

    controller = module.get<ForestryController>(ForestryController);
    service = module.get<ForestryService>(ForestryService);
    jest.clearAllMocks();
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
  describe('findAll', () => {
    describe('when findAll is called', () => {
      let result: Forestry[];
      let stub: ReturnType<typeof forestryStub>;
      beforeEach(async () => {
        stub = forestryStub();
        result = await controller.findAll();
      });
      it('should call service method', () => {
        expect(service.findAll).toHaveBeenCalled();
      });
      it('should return an array of forestries', async () => {
        expect(result).toEqual([stub]);
      });
    });
  });
  describe('findOne', () => {
    describe('when findOne is called', () => {
      let result: Forestry;
      let stub: ReturnType<typeof forestryStub>;
      beforeEach(async () => {
        stub = forestryStub();
        result = await controller.findById(stub.district);
      });
      it('should call service method', () => {
        expect(service.findById).toHaveBeenCalledWith(stub.district);
      });
      it('should return forestry', async () => {
        expect(result).toEqual(stub);
      });
    });
  });
  describe('remove', () => {
    describe('when remove is called', () => {
      let result: Forestry;
      let stub: ReturnType<typeof forestryStub>;
      beforeEach(async () => {
        stub = forestryStub();
        result = await controller.remove(stub.district);
      });
      it('should call service method', () => {
        expect(service.remove).toHaveBeenCalledWith(stub.district);
      });
      it('should return forestry', async () => {
        expect(result).toEqual(stub);
      });
    });
  });
  describe('create', () => {
    describe('when create is called', () => {
      let result: Forestry;
      let createForestryDto: CreateForestryDto;
      let stub: ReturnType<typeof forestryStub>;
      beforeEach(async () => {
        stub = forestryStub();
        createForestryDto = {
          districtId: stub.district,
          coords: stub.coords,
          name: stub.name,
        };
        result = await controller.create(createForestryDto);
      });
      it('should call service method', () => {
        expect(service.create).toHaveBeenCalledWith(createForestryDto);
      });
      it('should return a news', async () => {
        expect(result).toEqual(stub);
      });
    });
  });
  describe('update', () => {
    describe('when update is called', () => {
      let result: Forestry;
      let updateForestryDto: UpdateForestryDto;
      let stub: ReturnType<typeof forestryStub>;
      beforeEach(async () => {
        stub = forestryStub();
        updateForestryDto = {
          coords: stub.coords,
        };
        result = await controller.update(stub.district, updateForestryDto);
      });
      it('should call service method', () => {
        expect(service.update).toHaveBeenCalledWith(
          stub.district,
          updateForestryDto,
        );
      });
      it('should return a news', async () => {
        expect(result).toEqual(stub);
      });
    });
  });
});
