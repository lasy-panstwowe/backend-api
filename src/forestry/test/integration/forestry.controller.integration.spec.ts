import { Test } from '@nestjs/testing';
import { AppModule } from '../../../app.module';
import { DatabaseService } from '../../../database/database.service';
import { Connection, Types } from 'mongoose';
import * as request from 'supertest';
import { forestryStub } from '../stubs/forestry.stub';
import { forestryInspectorateStub } from '../../../forestry_inspectorate/test/stubs/forestry_inspectorate.stub';
import { districtStub } from '../../../district/test/stubs/district.stub';

describe('NewsController', () => {
  let dbConnection: Connection;
  let httpServer: any;
  let app: any;

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();
    app = moduleRef.createNestApplication();
    await app.init();
    dbConnection = moduleRef
      .get<DatabaseService>(DatabaseService)
      .getDbHandle();
    httpServer = app.getHttpServer();
  });

  afterEach(async () => {
    await dbConnection.collection('forestries').deleteMany({});
  });

  afterAll(async () => {
    await app.close();
  });

  describe('findById', () => {
    it('should return forestry matched by id', async () => {
      const stub = forestryStub();
      const forestry = await dbConnection.collection('forestries').insertOne({
        ...stub,
        _id: new Types.ObjectId(),
      });
      const response = await request(httpServer).get(
        `/forestry/${forestry.insertedId}`,
      );
      expect(response.status).toBe(200);
      expect(response.body).toMatchObject(stub);
    });
  });

  describe('findAll', () => {
    it('should return an array of forestries', async () => {
      const stub = forestryStub();
      await dbConnection.collection('forestries').insertOne(stub);
      const response = await request(httpServer).get('/forestry');
      expect(response.status).toBe(200);
      expect(response.body).toMatchObject([stub]);
    });
  });

  describe('create', () => {
    it('should create new forestry', async () => {
      const stub = forestryStub();
      const forestryInspectorate = await dbConnection
        .collection('forestryinspectorates')
        .insertOne({
          ...forestryInspectorateStub(),
          _id: new Types.ObjectId(),
        });
      const district = await dbConnection.collection('districts').insertOne({
        ...districtStub(),
        forestryInspectorate: forestryInspectorate.insertedId,
        _id: new Types.ObjectId(),
      });
      const createForestryDto = {
        coords: stub.coords,
        districtId: district.insertedId,
        name: stub.name,
      };
      const response = await request(httpServer)
        .post('/forestry')
        .send(createForestryDto);
      expect(response.status).toBe(201);
      const responseDto = {
        coords: createForestryDto.coords,
        district: createForestryDto.districtId,
        name: createForestryDto.name,
      };
      expect(response.body).toMatchObject(responseDto);
      const forestry = await dbConnection
        .collection('forestries')
        .findOne({ name: createForestryDto.name });
      expect(forestry).toMatchObject(responseDto);
    });
  });
});
