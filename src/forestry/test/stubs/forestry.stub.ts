import { Forestry } from 'src/forestry/schemas/forestry.schema';

export const forestryStub = (): Forestry => {
  return {
    name: 'forestry stub',
    coords: [
      {
        lat: 50,
        lng: 19,
      },
      {
        lat: 17,
        lng: 48,
      },
      {
        lat: 21,
        lng: 37,
      },
    ],
    district: '6260182db887cd308cdaaa19',
  };
};
