import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { Document } from 'mongoose';
import { DistrictCollectionName } from '../../district/schemas/district.schema';

export const ForestryCollectionName = 'Forestry';

export type ForestryDocument = Forestry & Document;

@Schema()
export class Point {
  @Prop({ required: true })
  lat: number;
  @Prop({ required: true })
  lng: number;
}

@Schema()
export class Forestry {
  @Prop({ required: true })
  name: string;
  @Prop()
  coords: Point[];
  @Prop({
    required: true,
    ref: DistrictCollectionName,
    type: mongoose.Schema.Types.ObjectId,
  })
  district: string;
}

export const ForestrySchema = SchemaFactory.createForClass(Forestry);
