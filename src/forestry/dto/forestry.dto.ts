import { IsString } from 'class-validator';
import { Point } from '../schemas/forestry.schema';

export class ForestryDto {
  @IsString()
  id: string;
  @IsString()
  districtId: string;
  @IsString()
  name: string;
  coords: Point[];
}
