import { IsMongoId, IsString, ValidateNested } from 'class-validator';
import { Point } from '../schemas/forestry.schema';

export class CreateForestryDto {
  @IsMongoId()
  districtId: string;
  @IsString()
  name: string;
  @ValidateNested()
  coords: Point[];
}
