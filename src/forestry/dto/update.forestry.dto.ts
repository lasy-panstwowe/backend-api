import { PartialType } from '@nestjs/mapped-types';
import { CreateForestryDto } from './create.forestry.dto';

export class UpdateForestryDto extends PartialType(CreateForestryDto) {}
