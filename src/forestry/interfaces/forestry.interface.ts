import { CreateForestryDto } from '../dto/create.forestry.dto';
import { UpdateForestryDto } from '../dto/update.forestry.dto';
import { ForestryDocument } from '../schemas/forestry.schema';

export interface IForestry {
  create(createForestryDto: CreateForestryDto): Promise<ForestryDocument>;
  findAll(): Promise<ForestryDocument[]>;
  findById(id: string): Promise<ForestryDocument>;
  update(
    id: string,
    updateForestryDto: UpdateForestryDto,
  ): Promise<ForestryDocument>;
  remove(id: string): Promise<ForestryDocument>;
}
