import { forestryStub } from '../test/stubs/forestry.stub';

export const ForestryService = jest.fn().mockReturnValue({
  findAll: jest.fn().mockResolvedValue([forestryStub()]),
  findById: jest.fn().mockResolvedValue(forestryStub()),
  create: jest.fn().mockResolvedValue(forestryStub()),
  update: jest.fn().mockResolvedValue(forestryStub()),
  remove: jest.fn().mockResolvedValue(forestryStub()),
});
