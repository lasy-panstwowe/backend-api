import { forestryStub } from '../test/stubs/forestry.stub';

export const ForestryRepository = jest.fn().mockReturnValue({
  findById: jest.fn().mockResolvedValue(forestryStub()),
  findByCoords: jest.fn().mockResolvedValue(forestryStub()),
  find: jest.fn().mockResolvedValue([forestryStub()]),
  findByIdAndDelete: jest.fn().mockResolvedValue(forestryStub()),
  create: jest.fn().mockResolvedValue(forestryStub()),
  update: jest.fn().mockResolvedValue(forestryStub()),
  remove: jest.fn().mockResolvedValue(forestryStub()),
});
