import {
  BadRequestException,
  InternalServerErrorException,
} from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { DistrictService } from '../district/district.service';
import { CreateForestryDto } from './dto/create.forestry.dto';
import { ForestryRepository } from './forestry.repository';
import { ForestryService } from './forestry.service';
import { Forestry, ForestryDocument } from './schemas/forestry.schema';
import { forestryStub } from './test/stubs/forestry.stub';
jest.mock('./forestry.repository');
jest.mock('../district/district.service');

describe('ForestryService', () => {
  let service: ForestryService;
  let repository: ForestryRepository;
  let districtService: DistrictService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ForestryService, ForestryRepository, DistrictService],
    }).compile();

    service = module.get<ForestryService>(ForestryService);
    districtService = module.get<DistrictService>(DistrictService);
    repository = module.get<ForestryRepository>(ForestryRepository);
  });
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
  describe('findAll', () => {
    describe('when findAll is called', () => {
      let result: Forestry[];
      let stub: ReturnType<typeof forestryStub>;
      beforeEach(async () => {
        stub = forestryStub();
        result = await service.findAll();
      });
      it('should call repository method', () => {
        expect(repository.find).toHaveBeenCalled();
      });
      it('should return an array of forestries', async () => {
        expect(result).toEqual([stub]);
      });
    });
  });
  describe('remove', () => {
    describe('when remove is called', () => {
      let result: Forestry;
      let stub: ReturnType<typeof forestryStub>;
      beforeEach(async () => {
        stub = forestryStub();
        result = await service.remove(stub.district);
      });
      it('should call repository method', () => {
        expect(repository.findByIdAndDelete).toHaveBeenCalledWith(
          stub.district,
        );
      });
      it('should return forestry', async () => {
        expect(result).toEqual(stub);
      });
    });
  });
  describe('findOne', () => {
    describe('when findOne is called', () => {
      let result: Forestry;
      let stub: ReturnType<typeof forestryStub>;
      beforeEach(async () => {
        stub = forestryStub();
        result = await service.findById(stub.district);
      });
      it('should call repository method', () => {
        expect(repository.findById).toHaveBeenCalledWith(stub.district);
      });
      it('should return forestry', async () => {
        expect(result).toEqual(stub);
      });
    });
  });
  describe('create', () => {
    let createForestryDto: CreateForestryDto;
    let stub: ReturnType<typeof forestryStub>;
    beforeEach(async () => {
      stub = forestryStub();
      createForestryDto = {
        coords: stub.coords,
        districtId: stub.district,
        name: stub.name,
      };
    });
    describe('should throw exception', () => {
      it('if lat is less than -90', () => {
        createForestryDto.coords[0].lat = -91;
        expect(service.create(createForestryDto)).rejects.toThrow(
          BadRequestException,
        );
      });
      it('if lat is more than 90', () => {
        createForestryDto.coords[0].lat = 91;
        expect(service.create(createForestryDto)).rejects.toThrow(
          BadRequestException,
        );
      });
      it('if lng is more than 180', () => {
        createForestryDto.coords[0].lng = 181;
        expect(service.create(createForestryDto)).rejects.toThrow(
          BadRequestException,
        );
      });
      it('if lng is less than -180', () => {
        createForestryDto.coords[0].lng = -181;
        expect(service.create(createForestryDto)).rejects.toThrow(
          BadRequestException,
        );
      });
      it('if created forestry has two equal coordination points', () => {
        createForestryDto.coords[1] = createForestryDto.coords[0];
        expect(service.create(createForestryDto)).rejects.toThrow(
          BadRequestException,
        );
      });
      it('if provided district does not exist', () => {
        jest.spyOn(districtService, 'findById').mockImplementation(() => null);
        expect(service.create(createForestryDto)).rejects.toThrow(
          InternalServerErrorException,
        );
      });
      it('if there is already a forestry with provided coords', () => {
        expect(service.create(createForestryDto)).rejects.toThrow(
          InternalServerErrorException,
        );
      });
      it('if less than 3 coordination points were provided', () => {
        createForestryDto = {
          ...createForestryDto,
          coords: [],
        };
        expect(service.create(createForestryDto)).rejects.toThrow(
          BadRequestException,
        );
      });
    });
  });

  describe('findByCoords', () => {
    describe('when findByCoords is called', () => {
      let result: ForestryDocument;
      let stub: Forestry;
      beforeEach(async () => {
        stub = forestryStub();
        jest.clearAllMocks();
      });
      it('should call repository method', async () => {
        result = await service.findByCoords(stub.coords);
        expect(repository.findByCoords).toHaveBeenCalledWith(stub.coords);
      });
      it('should return forestry if it exists', async () => {
        result = await service.findByCoords(stub.coords);
        expect(result).toEqual(stub);
      });
      it('should return null if forestry does not exist', async () => {
        jest.spyOn(repository, 'findByCoords').mockImplementation(() => null);
        result = await service.findByCoords(stub.coords);
        expect(result).toEqual(null);
      });
    });
  });
});
