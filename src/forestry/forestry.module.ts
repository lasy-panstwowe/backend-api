import { Module } from '@nestjs/common';
import { ForestryService } from './forestry.service';
import { ForestryController } from './forestry.controller';
import { DistrictModule } from '../district/district.module';
import { MongooseModule } from '@nestjs/mongoose';
import { Forestry, ForestrySchema } from './schemas/forestry.schema';
import { ForestryRepository } from './forestry.repository';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Forestry.name, schema: ForestrySchema },
    ]),
    DistrictModule,
  ],
  controllers: [ForestryController],
  providers: [ForestryService, ForestryRepository],
  exports: [ForestryService],
})
export class ForestryModule {}
