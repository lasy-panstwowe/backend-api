import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { MongoRepository } from '../database/repositories/mongo.repository';
import { Model, Types } from 'mongoose';
import { User, UserDocument } from './schemas/user.schema';
import { CreateUserDto } from './dto/create-user.dto';

@Injectable()
export class UserRepository extends MongoRepository<UserDocument> {
  constructor(
    @InjectModel(User.name)
    private readonly userModel: Model<UserDocument>,
  ) {
    super(userModel);
  }

  findByUsername(username: string) {
    return this.userModel.findOne({ username }).populate('person');
  }

  findAll() {
    return this.userModel.find().populate('person');
  }

  createUser(createUserDto: CreateUserDto) {
    const {
      username,
      name,
      surname,
      employeeType,
      phone,
      email,
      password,
      forestryId,
    } = createUserDto;
    const person = {
      name,
      surname,
      employeeType,
      phone,
      email,
      forestry: new Types.ObjectId(forestryId),
    };
    return this.create({
      username,
      password,
      person: person,
    });
  }
}
