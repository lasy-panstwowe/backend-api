import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Person } from '../../person/schemas/person.schema';
import mongoose, { Document } from 'mongoose';

export type UserDocument = User & Document;

@Schema()
export class User {
  @Prop({ required: true })
  username: string;
  @Prop({ required: true })
  password: string;
  @Prop({
    required: true,
    ref: Person.name,
    type: mongoose.Schema.Types.ObjectId,
  })
  person: Person;
}

export const UserSchema = SchemaFactory.createForClass(User);
