import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UserRepository } from './user.repository';
import * as bcrypt from 'bcrypt';
import { PersonService } from 'src/person/person.service';

@Injectable()
export class UserService {
  constructor(
    private readonly userRepository: UserRepository,
    private readonly personService: PersonService,
  ) {}

  async create(createUserDto: CreateUserDto) {
    const {
      username,
      name,
      phone,
      email,
      forestryId,
      surname,
      employeeType,
      password,
    } = createUserDto;
    const duplicateUser = await this.findByUsername(username);
    if (duplicateUser)
      throw new InternalServerErrorException(
        `User with username: ${username} already exists`,
      );
    const encryptedPassword = bcrypt.hashSync(password, 10);
    const person = await this.personService.create({
      name,
      surname,
      phone,
      email,
      employeeType,
      forestryId,
    });

    return this.userRepository.create({
      username,
      password: encryptedPassword,
      person: person._id,
    });
  }

  findByUsername(username: string) {
    return this.userRepository.findByUsername(username);
  }
  findById(id: string) {
    return this.userRepository.findById(id);
  }
  findAll() {
    return this.userRepository.findAll();
  }
}
