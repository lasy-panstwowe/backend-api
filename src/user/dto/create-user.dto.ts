import { Exclude } from 'class-transformer';
import {
  IsEmail,
  IsISO8601,
  IsMobilePhone,
  IsMongoId,
  IsString,
  Matches,
  MaxLength,
} from 'class-validator';
import { EmployeeType } from 'src/person/schemas/person.schema';

export class CreateUserDto {
  @IsString()
  @MaxLength(20)
  username: string;
  @IsString()
  @MaxLength(20)
  name: string;
  @IsString()
  @MaxLength(30)
  surname: string;
  @Matches(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/, {
    message: 'password too weak',
  })
  password: string;
  @IsEmail()
  email: string;
  @IsMobilePhone()
  phone: string;
  employeeType: EmployeeType;
  @IsMongoId()
  forestryId: string;
}
