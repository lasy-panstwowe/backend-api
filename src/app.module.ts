import { Module } from '@nestjs/common';
import { DistrictModule } from './district/district.module';
import { ForestryModule } from './forestry/forestry.module';
import { ForestryInspectorateModule } from './forestry_inspectorate/forestry_inspectorate.module';
import { DeviceModule } from './device/device.module';
import { ConfigModule } from '@nestjs/config';
import { DatabaseModule } from './database/database.module';
import { UserModule } from './user/user.module';
import { AuthModule } from './auth/auth.module';
import { RegistrationModule } from './registration/registration.module';
import { PersonModule } from './person/person.module';
@Module({
  imports: [
    ForestryModule,
    DistrictModule,
    ForestryInspectorateModule,
    DeviceModule,
    DatabaseModule,
    ConfigModule.forRoot({ isGlobal: true }),
    UserModule,
    AuthModule,
    RegistrationModule,
    PersonModule,
  ],
})
export class AppModule {}
