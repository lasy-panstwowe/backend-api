import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { CreateDeviceDto } from './dto/create.device.dto';
import { DataDeviceDto } from './dto/data.device.dto';
import { DeviceRepository } from './device.repository';
import { IDevice } from './interfaces/device.interface';
import { DeviceType } from './schemas/device.schema';

@Injectable()
export class DeviceService implements IDevice {
  constructor(private readonly deviceRepository: DeviceRepository) { }
  async create(createDeviceDto: CreateDeviceDto) {
    return this.deviceRepository.createDevice(createDeviceDto);
  }

  async insertData(id: string, dataDeviceDto: DataDeviceDto) {
    const device = await this.deviceRepository.findById(id);
    if (!device) throw new InternalServerErrorException('Device not found');
    if (dataDeviceDto.type == DeviceType.Fire) {
      if (dataDeviceDto.temperature && dataDeviceDto.smoke && dataDeviceDto.humidity && dataDeviceDto.battery) {
        return this.deviceRepository.insertFireData(id, dataDeviceDto);
      } else {
        throw new InternalServerErrorException('Missing fire data');
      }
    }
    else{
      if (dataDeviceDto.battery && dataDeviceDto.coord){
        return this.deviceRepository.insertAnimalData(id, dataDeviceDto);
      } else {
        throw new InternalServerErrorException("Some of the animal data is missing!")
      }
    }
  }

  async insertEvent(id: string, payload: any) {
    const device = await this.deviceRepository.findById(id);
    if (!device) throw new InternalServerErrorException('Device not found');
    return this.deviceRepository.insertEvent(id, payload);
  }

  findAll() {
    return this.deviceRepository.find();
  }

  findById(id: string) {
    return this.deviceRepository.findById(id);
  }

  remove(id: string) {
    return this.deviceRepository.findByIdAndDelete(id);
  }
}
