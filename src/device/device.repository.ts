import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { MongoRepository } from '../database/repositories/mongo.repository';
import { Types, Model } from 'mongoose';
import { Device, DeviceDocument } from './schemas/device.schema';
import { CreateDeviceDto } from './dto/create.device.dto';
import { DataDeviceDto } from './dto/data.device.dto';

@Injectable()
export class DeviceRepository extends MongoRepository<DeviceDocument> {
  constructor(
    @InjectModel(Device.name)
    private readonly deviceModel: Model<DeviceDocument>,
  ) {
    super(deviceModel);
  }
  createDevice(createDeviceDto: CreateDeviceDto) {
    const { name, ...rest } = createDeviceDto;
    return this.create({
      name,
      data: [],
      ...rest,
      _id: new Types.ObjectId(),
    });
  }
  insertFireData(id: string, data: DataDeviceDto) {
    return this.findByIdAndUpdate(id, {
      $push: { fireData: data },
      lastTime: Date.now(),
    });
  }
  insertAnimalData(id: string, data: DataDeviceDto) {
    return this.findByIdAndUpdate(id, {
      $push: { animalData: data },
      lastTime: Date.now(),
    });
  }
  insertEvent(id: string, payload: any) {
    return this.findByIdAndUpdate(id, {
      $push: { events: payload },
      lastTime: Date.now()
    });
  }
}
