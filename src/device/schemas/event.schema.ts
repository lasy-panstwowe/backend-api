import { Schema, Prop, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type DeviceEventDocument = DeviceEvent & Document;


@Schema()
export class DeviceEvent {
    @Prop({ required: true })
    timestamp: Date;
    @Prop({ required: true })
    payload: any;
}

export const DeviceEventSchema = SchemaFactory.createForClass(DeviceEvent);
