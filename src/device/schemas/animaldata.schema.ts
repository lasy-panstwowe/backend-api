import { Schema, Prop, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { Point } from '../../forestry/schemas/forestry.schema';

export type AnimalDataDocument = AnimalData & Document;

@Schema()
export class AnimalData {
  @Prop({ required: true })
  timestamp: Date;
  @Prop({ required: true })
  battery: number;
  @Prop({ required: true })
  coord: Point;
}

export const AnimalDataSchema = SchemaFactory.createForClass(AnimalData);
