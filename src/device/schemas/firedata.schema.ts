import { Schema, Prop, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { Document } from 'mongoose';
import { ForestryCollectionName } from '../../forestry/schemas/forestry.schema';

export type FireDataDocument = FireData & Document;

@Schema()
export class FireData {
  @Prop({ required: true })
  timestamp: Date;
  @Prop({ required: true })
  temperature: number;
  @Prop({ required: true })
  smoke: number;
  @Prop({ required: true })
  humidity: number;
  @Prop({ required: true })
  battery: number;
}

export const FireDataSchema = SchemaFactory.createForClass(FireData);
