import { Schema, Prop, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { Document } from 'mongoose';
import {
  ForestryCollectionName,
  Point,
} from '../../forestry/schemas/forestry.schema';
import { AnimalData, AnimalDataSchema } from './animaldata.schema';
import { DeviceEvent } from './event.schema';
import { FireData, FireDataSchema } from './firedata.schema';
export const DeviceCollectionName = 'Device';

export type DeviceDocument = Device & Document;

export enum DeviceType {
  Fire = 'Fire',
  Animal = 'Animal',
}

@Schema()
export class Device {
  @Prop({ required: true })
  name: string;
  @Prop({ required: true, enum: [DeviceType.Fire, DeviceType.Animal] })
  type: string;
  @Prop()
  defaultCoords: Point; // only for stationary sensors
  @Prop({
    required: true,
    ref: ForestryCollectionName,
    type: mongoose.Schema.Types.ObjectId,
  })
  forestry: string;
  @Prop()
  sendFreq: number;
  @Prop()
  sampleFreq: number;
  @Prop()
  lastTime: Date;
  @Prop()
  events: DeviceEvent[];
  @Prop({ required: true, type: [FireDataSchema] })
  fireData: FireData[];
  @Prop({ required: true, type: [AnimalDataSchema] })
  animalData: AnimalData[];
}

export const DeviceSchema = SchemaFactory.createForClass(Device);
