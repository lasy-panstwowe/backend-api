import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  MessageEvent,
  Sse,
} from '@nestjs/common';
import { CreateDeviceDto } from './dto/create.device.dto';
import { DeviceService } from './device.service';
import { DataDeviceDto } from './dto/data.device.dto';
import { Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';

@Controller('device')
export class DeviceController {
  emergencyQueue: Subject<[string, any]>;

  constructor(private readonly deviceService: DeviceService) {
    this.emergencyQueue = new Subject<[string, any]>();
  }

  @Post()
  create(@Body() createDeviceDto: CreateDeviceDto) {
    return this.deviceService.create(createDeviceDto);
  }

  @Post(':id/data')
  insertData(@Body() dataDeviceDto: DataDeviceDto, @Param('id') id: string) {
    return this.deviceService.insertData(id, dataDeviceDto);
  }

  @Post(':id/emergency')
  emergency(@Param('id') id: string, @Body() payload: any) {
    this.deviceService.insertEvent(id, payload);
    this.emergencyQueue.next([id, payload]);
    return '';
  }

  @Sse('on-emergency')
  opEmergency(): Observable<MessageEvent> {
    return this.emergencyQueue.pipe(map((d) => ({ data: { id: d[0], payload: d[1] } })));
  }

  @Get()
  findAll() {
    return this.deviceService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.deviceService.findById(id);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.deviceService.remove(id);
  }
}
