import { CreateDeviceDto } from '../dto/create.device.dto';
import { DataDeviceDto } from '../dto/data.device.dto';
import { DeviceDocument } from '../schemas/device.schema';

export interface IDevice {
  create(createDeviceDto: CreateDeviceDto): Promise<DeviceDocument>;
  findAll(): Promise<DeviceDocument[]>;
  findById(id: string): Promise<DeviceDocument>;
  remove(id: string): Promise<DeviceDocument>;
  insertData(id: string, dataDeviceDto: DataDeviceDto): Promise<DeviceDocument>;
}
