import { IsString, IsNumber, IsEnum } from 'class-validator';
import { Point } from '../../forestry/schemas/forestry.schema';
import { DeviceType } from '../schemas/device.schema';

export class CreateDeviceDto {
  @IsString()
  name: string;
  defaultCoords: Point;
  @IsEnum(DeviceType)
  type: string;
  @IsString()
  forestry: string;
  @IsNumber()
  sendFreq: number;
  @IsNumber()
  sampleFreq: number;
}
