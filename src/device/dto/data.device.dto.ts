import { IsNumber, IsOptional, IsDateString, IsEnum } from 'class-validator';
import { Point } from '../../forestry/schemas/forestry.schema';
import { DeviceType } from '../schemas/device.schema';

export class DataDeviceDto {
  @IsEnum(DeviceType)
  type: string;
  @IsDateString()
  timestamp: Date;
  @IsOptional()
  @IsNumber()
  temperature: number;
  @IsOptional()
  @IsNumber()
  smoke: number;
  @IsOptional()
  @IsNumber()
  humidity: number;
  @IsOptional()
  @IsNumber()
  battery: number;
  @IsOptional()
  coord: Point;
}
