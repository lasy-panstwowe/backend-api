import { IsString, IsNumber, IsEnum, IsOptional, isObject } from 'class-validator';
import { Point } from '../../forestry/schemas/forestry.schema';
import { DeviceType } from '../schemas/device.schema';

export class DeviceDto {
  @IsString()
  name: string;
  defaultCoords: Point;
  @IsEnum(DeviceType)
  type: string;
  @IsString()
  forestry: string;
  @IsNumber()
  sendFreq: number;
  @IsNumber()
  sampleFreq: number;
  @IsOptional()
  events: any;
  @IsOptional()
  fireData: any;
  @IsOptional()
  animalData: any;
}
