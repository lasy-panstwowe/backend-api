export interface IJWTPayload {
  uid: string;
  username: string;
}
