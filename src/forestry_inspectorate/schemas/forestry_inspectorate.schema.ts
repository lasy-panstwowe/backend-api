import { Schema, Prop, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export const ForestryInspectorateCollectionName = 'ForestInspectorate';

export type ForestryInspectorateDocument = ForestryInspectorate & Document;

@Schema()
export class ForestryInspectorate {
  @Prop({ required: true })
  name: string;
}

export const ForestryInspectorateSchema =
  SchemaFactory.createForClass(ForestryInspectorate);
