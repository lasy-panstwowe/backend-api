import { Injectable } from '@nestjs/common';
import { CreateForestryInspectorateDto } from './dto/create-forestry_inspectorate.dto';
import { UpdateForestryInspectorateDto } from './dto/update-forestry_inspectorate.dto';
import { ForestryInspectorateRepository } from './forestry_inspectorate.repository';
import { IForestryInspectorate } from './interfaces/forestry_inspectorate.interface';

@Injectable()
export class ForestryInspectorateService implements IForestryInspectorate {
  constructor(
    private readonly forestryInspectorateRepository: ForestryInspectorateRepository,
  ) {}
  create(createForestryInspectorateDto: CreateForestryInspectorateDto) {
    return this.forestryInspectorateRepository.createForestyInspectorate(
      createForestryInspectorateDto,
    );
  }

  findAll() {
    return this.forestryInspectorateRepository.find();
  }

  findById(id: string) {
    return this.forestryInspectorateRepository.findById(id);
  }

  update(
    id: string,
    updateForestryInspectorateDto: UpdateForestryInspectorateDto,
  ) {
    return this.forestryInspectorateRepository.findByIdAndUpdate(
      id,
      updateForestryInspectorateDto,
    );
  }

  remove(id: string) {
    return this.forestryInspectorateRepository.findByIdAndDelete(id);
  }
}
