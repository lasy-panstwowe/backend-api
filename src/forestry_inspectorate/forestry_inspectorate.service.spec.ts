import { Test, TestingModule } from '@nestjs/testing';
import { ForestryInspectorateService } from './forestry_inspectorate.service';

describe('ForestryInspectorateService', () => {
  let service: ForestryInspectorateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ForestryInspectorateService],
    }).compile();

    service = module.get<ForestryInspectorateService>(
      ForestryInspectorateService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
