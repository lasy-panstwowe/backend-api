import { Test, TestingModule } from '@nestjs/testing';
import { ForestryInspectorateController } from './forestry_inspectorate.controller';
import { ForestryInspectorateRepository } from './forestry_inspectorate.repository';
import { ForestryInspectorateService } from './forestry_inspectorate.service';

describe('ForestryInspectorateController', () => {
  let controller: ForestryInspectorateController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ForestryInspectorateController],
      providers: [ForestryInspectorateService, ForestryInspectorateRepository],
    }).compile();

    controller = module.get<ForestryInspectorateController>(
      ForestryInspectorateController,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
