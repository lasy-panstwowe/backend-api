import { CreateForestryInspectorateDto } from '../dto/create-forestry_inspectorate.dto';
import { UpdateForestryInspectorateDto } from '../dto/update-forestry_inspectorate.dto';
import { ForestryInspectorateDocument } from '../schemas/forestry_inspectorate.schema';

export interface IForestryInspectorate {
  create(
    CreateForestryInspectorateDto: CreateForestryInspectorateDto,
  ): Promise<ForestryInspectorateDocument>;
  findAll(): Promise<ForestryInspectorateDocument[]>;
  findById(id: string): Promise<ForestryInspectorateDocument>;
  update(
    id: string,
    updateForestryDto: UpdateForestryInspectorateDto,
  ): Promise<ForestryInspectorateDocument>;
  remove(id: string): Promise<ForestryInspectorateDocument>;
}
