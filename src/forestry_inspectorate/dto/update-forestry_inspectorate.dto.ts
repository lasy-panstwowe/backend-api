import { PartialType } from '@nestjs/mapped-types';
import { CreateForestryInspectorateDto } from './create-forestry_inspectorate.dto';

export class UpdateForestryInspectorateDto extends PartialType(
  CreateForestryInspectorateDto,
) {}
