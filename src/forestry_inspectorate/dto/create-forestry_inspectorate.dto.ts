import { IsString } from 'class-validator';

export class CreateForestryInspectorateDto {
  @IsString()
  name: string;
}
