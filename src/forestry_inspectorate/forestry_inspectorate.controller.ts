import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { ForestryInspectorateService } from './forestry_inspectorate.service';
import { CreateForestryInspectorateDto } from './dto/create-forestry_inspectorate.dto';
import { UpdateForestryInspectorateDto } from './dto/update-forestry_inspectorate.dto';

@Controller('forestry-inspectorate')
export class ForestryInspectorateController {
  constructor(
    private readonly forestryInspectorateService: ForestryInspectorateService,
  ) {}

  @Post()
  create(@Body() createForestryInspectorateDto: CreateForestryInspectorateDto) {
    return this.forestryInspectorateService.create(
      createForestryInspectorateDto,
    );
  }

  @Get()
  findAll() {
    return this.forestryInspectorateService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.forestryInspectorateService.findById(id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateForestryInspectorateDto: UpdateForestryInspectorateDto,
  ) {
    return this.forestryInspectorateService.update(
      id,
      updateForestryInspectorateDto,
    );
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.forestryInspectorateService.remove(id);
  }
}
