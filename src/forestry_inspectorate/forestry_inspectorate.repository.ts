import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { MongoRepository } from '../database/repositories/mongo.repository';
import { Model, Types } from 'mongoose';
import {
  ForestryInspectorate,
  ForestryInspectorateDocument,
} from './schemas/forestry_inspectorate.schema';
import { CreateForestryInspectorateDto } from './dto/create-forestry_inspectorate.dto';

@Injectable()
export class ForestryInspectorateRepository extends MongoRepository<ForestryInspectorateDocument> {
  constructor(
    @InjectModel(ForestryInspectorate.name)
    private readonly forestryInspectorateModel: Model<ForestryInspectorateDocument>,
  ) {
    super(forestryInspectorateModel);
  }
  createForestyInspectorate(
    createForestryInspectorateDto: CreateForestryInspectorateDto,
  ) {
    return this.create({
      ...createForestryInspectorateDto,
      _id: new Types.ObjectId(),
    });
  }
}
