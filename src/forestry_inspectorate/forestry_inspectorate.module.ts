import { Module } from '@nestjs/common';
import { ForestryInspectorateService } from './forestry_inspectorate.service';
import { ForestryInspectorateController } from './forestry_inspectorate.controller';
import { ForestryInspectorateRepository } from './forestry_inspectorate.repository';
import {
  ForestryInspectorate,
  ForestryInspectorateSchema,
} from './schemas/forestry_inspectorate.schema';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: ForestryInspectorate.name, schema: ForestryInspectorateSchema },
    ]),
  ],
  controllers: [ForestryInspectorateController],
  providers: [ForestryInspectorateService, ForestryInspectorateRepository],
})
export class ForestryInspectorateModule {}
