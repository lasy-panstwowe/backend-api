import {
  IsString,
  MaxLength,
  IsEmail,
  IsMobilePhone,
  IsMongoId,
} from 'class-validator';
import { EmployeeType } from '../schemas/person.schema';

export class CreatePersonDto {
  @IsString()
  @MaxLength(20)
  name: string;
  @IsString()
  @MaxLength(30)
  surname: string;
  @IsEmail()
  email: string;
  @IsMobilePhone()
  phone: string;
  employeeType: EmployeeType;
  @IsMongoId()
  forestryId: string;
}
