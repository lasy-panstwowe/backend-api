import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { ForestryService } from 'src/forestry/forestry.service';
import { PersonRepository } from './person.repository';
import { CreatePersonDto } from './dto/create-person.dto';

@Injectable()
export class PersonService {
  constructor(
    private readonly personRepository: PersonRepository,
    private readonly forestryService: ForestryService,
  ) {}

  async create(createPersonDto: CreatePersonDto) {
    const { forestryId } = createPersonDto;
    const forestry = await this.forestryService.findById(forestryId);
    if (!forestry)
      throw new InternalServerErrorException(
        `Forestry with id: ${forestryId} does not exist`,
      );
    return this.personRepository.createPerson(createPersonDto);
  }
}
