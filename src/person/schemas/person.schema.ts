import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { Document } from 'mongoose';
import { Forestry } from '../../forestry/schemas/forestry.schema';

export enum EmployeeType {
  Non = 'non',
  Admin = 'admin',
  Manager = 'manager',
  Forester = 'forester',
  SubForester = 'subforester',
}

export type PersonDocument = Person & Document;

@Schema()
export class Person {
  @Prop({ required: true })
  name: string;
  @Prop({ required: true })
  surname: string;
  @Prop({ required: true })
  email: string;
  @Prop({ required: true })
  phone: string;
  @Prop({ required: true, enum: EmployeeType })
  employeeType: EmployeeType;
  @Prop({
    required: true,
    ref: Forestry.name,
    type: mongoose.Schema.Types.ObjectId,
  })
  forestry: Forestry;
}

export const PersonSchema = SchemaFactory.createForClass(Person);
