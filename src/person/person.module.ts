import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ForestryModule } from 'src/forestry/forestry.module';
import { PersonRepository } from './person.repository';
import { PersonService } from './person.service';
import { Person, PersonSchema } from './schemas/person.schema';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Person.name, schema: PersonSchema }]),
    ForestryModule,
  ],
  providers: [PersonService, PersonRepository],
  exports: [PersonService],
})
export class PersonModule {}
