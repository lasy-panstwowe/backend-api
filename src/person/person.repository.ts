import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { MongoRepository } from '../database/repositories/mongo.repository';
import { Model, Types } from 'mongoose';
import { Person, PersonDocument } from './schemas/person.schema';
import { CreatePersonDto } from './dto/create-person.dto';

@Injectable()
export class PersonRepository extends MongoRepository<PersonDocument> {
  constructor(
    @InjectModel(Person.name)
    private readonly personModel: Model<PersonDocument>,
  ) {
    super(personModel);
  }
  createPerson(createPersonDto: CreatePersonDto) {
    const { name, surname, email, phone, employeeType, forestryId } =
      createPersonDto;
    return this.create({
      name,
      surname,
      email,
      phone,
      employeeType,
      forestry: new Types.ObjectId(forestryId),
    });
  }
}
