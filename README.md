## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

# Emergency with simulator

Start backend:
```bash
$ npm run start:dev
```


Start example emergency client server:
```bash
$ cd tools

# start local http server for client
$ http-server -p 8000
# or
# python3 -m http.server
```

Open web browser, disable CORS and go to `localhost:8000/emergency_client.html`.

Run simulator in new terminal:

```bash
$ cd tools

$ node device_sim.js
Send emergency or data?[e/d]

```

Input 'e' and press enter.

Look into web browser it should show emergency info.
