const { request } = require('http');
const readline = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout
});

const id = process.env.FORESTRY_DEVICE_ID ?? '6276904b7bb42d14c085946e';
const host = process.env.FORESTRY_HOST ?? 'localhost';
const port = process.env.FORESTRY_PORT ?? '8080';
const interval = process.env.FORESTRY_DEVICE_INTERVAL ?? -1;
const type = process.env.FORESTRY_DEVICE_TYPE ?? "Fire";

let genPayload = () => {
    const timestamp = (new Date()).toISOString();
    const temperature = Math.random() * 30.0;
    const smoke = Math.random() * 10.0;
    const humidity = Math.random() * 100.0;
    const battery = Math.random() * 100.0;
    const lat = Math.random() * 100.0;
    const lng = Math.random() * 100.0;
    const coord = { lat, lng };

    const data = JSON.stringify({
        type,
        timestamp,
        battery,
        temperature: type == "Fire" ? temperature : undefined,
        smoke: type == "Fire" ? smoke : undefined,
        humidity: type == "Fire" ? humidity : undefined,
        coord: type == "Animal" ? coord : undefined,
    });

    return data;
}

let send = (url, data) => {
    const options = {
        host,
        port,
        method: 'POST',
        path: url,
        headers: {
            'Content-Type': 'application/json',
            'Content-Length': data.length
        }
    };
    readline.write("I'm posting following data: \n");
    readline.write(data);
    readline.write("\n");
    const req = request(options);
    req.write(data);
    req.end();
}

let sendData = () => send(`/device/${id}/data`, genPayload());

let sendEmergency = () => send(`/device/${id}/emergency`, genPayload());


if (interval <= 0) {
    readline.question(
        "Send emergency or data?[e/d]",
        (choice) => {
            if (choice === 'e') sendEmergency();
            else if (choice === 'd') sendData();
            readline.close()
        });
}
else {
    setInterval(() => { sendData() }, 1000 * interval);
}